import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Simpca extends JFrame implements ActionListener{
	Boolean numInput = true;
	JTextField textField;
	Double res = new Double(0);
	String prevAction;
	
	public Simpca(){
		//������ ���������� ���
		Font font = new Font("", Font.PLAIN, 20);
		/*JTextField*/ textField = new JTextField("", 7); 
		textField.setHorizontalAlignment(JTextField.RIGHT);
		textField.setFont(font);
		
		JPanel actionPanel = new JPanel();
		actionPanel.setLayout(new GridLayout(5, 1, 2, 2));
		String[] actionCaption = {"/", "*", "-", "+", "="};
		for (int i=0; i<actionCaption.length; i++){
			JButton button = new JButton(actionCaption[i]);
			button.addActionListener(this);
			actionPanel.add(button);
		}
		
		JPanel numPanel = new JPanel();
		numPanel.setLayout(new GridLayout(3, 3, 2, 2));
		String[] numCaption = {"9", "8", "7", "6", "5", "4", "3", "2", "1"};
		for (int i=0; i<numCaption.length; i++){
			JButton button = new JButton(numCaption[i]);
			button.addActionListener(this);
			numPanel.add(button);
		}
		
		JButton numZeroButton = new JButton("0");
		numZeroButton.addActionListener(this);
		
		JButton dotButton = new JButton(".");
		dotButton.addActionListener(this);
		
		JPanel numPad = new JPanel(new BorderLayout(2, 2));
		numPad.add(numPanel);
		numPad.add(numZeroButton, BorderLayout.SOUTH);
		
		JPanel form = new JPanel();
		form.setLayout(new BorderLayout(2, 2));
		form.add(textField,BorderLayout.NORTH);
		form.add(numPad, BorderLayout.WEST);
		form.add(actionPanel, BorderLayout.EAST);
		form.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		
		this.setContentPane(form);
		this.pack();
		this.setResizable(false);
		
	}
	
	public static void main(String[] args) {
		Simpca simpca = new Simpca();
		simpca.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		simpca.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//
		String action = e.getActionCommand().toString();
		switch (action) {
		case "9":
		case "8":
		case "7":
		case "6":
		case "5":
		case "4":
		case "3":
		case "2":
		case "1":
		case "0":
			if (numInput) {
				numInput = false;
				textField.setText(action);
			}
			else{
				if (textField.getText().equals("0")) textField.setText("");
				textField.setText(textField.getText() + action);
			}
			break;
		case "+":
			res = Double.parseDouble(textField.getText());
			textField.setText("0");
			prevAction = "+";
			numInput = true;
			break;
		case "-":
			res = Double.parseDouble(textField.getText());
			textField.setText("0");
			prevAction = "-";
			numInput = true;
			break;
		case "/":
			res = Double.parseDouble(textField.getText());
			textField.setText("0");
			prevAction = "/";
			numInput = true;
			break;
		case "*":
			res = Double.parseDouble(textField.getText());
			textField.setText("0");
			prevAction = "*";
			numInput = true;
			break;
		case "=":
			switch (prevAction) {
			case "+":
					res += Double.parseDouble(textField.getText());				
				break;
			case "-":
					res -= Double.parseDouble(textField.getText());
				break;
			case "*":
					res *= Double.parseDouble(textField.getText());
				break;
			case "/":
					res /= Double.parseDouble(textField.getText());
				break;
			}
			textField.setText(res.toString());
			numInput = true;
			break;
		}
	}
}

